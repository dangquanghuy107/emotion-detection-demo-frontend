import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Welcome from '@/components/Welcome'
import Enroll from '@/components/Enroll'

Vue.use(Router)

var router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/welcome',
            name: 'Welcome',
            component: Welcome, 
            props: true
        },
        {
            path: '/enroll',
            name: 'Enroll', 
            component: Enroll
        }
    ]
})

export default router