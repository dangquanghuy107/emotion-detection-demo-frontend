import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import Router from 'vue-router'
import router from './router/index'

import 'buefy/dist/buefy.css'

Vue.config.productionTip = false
Vue.use(Buefy)
Vue.use(Router)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
